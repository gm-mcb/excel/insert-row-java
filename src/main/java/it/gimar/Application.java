package it.gimar;

import com.jsoniter.output.JsonStream;
import it.gimar.fs.ProcessDir;
import it.gimar.xlsx.InsertRow;
import it.gimar.xlsx.RemoveRow;
import it.gimar.xlsx.convert.ToJson;
import it.gimar.xlsx.exception.NotAValidFileException;
import it.gimar.xlsx.exception.NotAnXlsxException;
import lombok.extern.java.Log;
import org.apache.commons.cli.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


@Log
public class Application {
    public static void main(String[] args) throws ParseException, IOException, NotAValidFileException, NotAnXlsxException, ProcessDir.NotADirectoryException {
        // create Options object
        Options options = new Options();
        options.addOption("d","dir", true, "directory to process");
        options.addOption("f","file", true, "file to process");
        options.addOption("j", "json", true, "output as json");
        options.addOption("s", true, "sheet to operate on");
        options.addOption("r", false, "remove empty row");
        options.addOption("?", "help", false, "display help");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse( options, args);

        if(cmd.getOptions().length == 0 || cmd.hasOption("?")){
            printHelp(options);
            System.exit(1);
        }
        String file = cmd.getOptionValue("f");
        String directory = cmd.getOptionValue("d");
        if( null == file && null == directory){
            log.severe("File (-f) or Directory (-d) parameters are required");
            System.exit(1);
        }
        if( null != file && null != directory){
            log.severe("Must use either File (-f) or Directory (-d) parameter, not both");
            System.exit(1);
        }
        Boolean remove = cmd.hasOption("r");
        String sheet = cmd.getOptionValue("s");

        Operation operation = Operation.INSERT_ROW;
        if(cmd.hasOption("r")){
            operation = Operation.REMOVE_ROW;
        }else if(cmd.hasOption("j")){
            operation = Operation.CONVERT_GM_TRANSLATE_JSON;
            String outfile = cmd.getOptionValue("j");
            if(null == outfile || "" == outfile){
                printHelp(options);
                System.exit(1);
            }
            Path parent = Paths.get(outfile).getParent();
            if(!Files.exists(parent) || !Files.isDirectory(parent)){
                log.warning("Folder: " + parent.toAbsolutePath().toString() + " does not exist");
                if( !Files.exists(parent)) {
                    parent.toFile().mkdirs();
                }
            }
        }
        if (file != null){
            if(Files.exists(Paths.get(file))){
                log.info("File: " + file);
                Path path = Paths.get(file);
                process(path, sheet, operation, cmd);
            }
            else {
                log.severe("File: " + file + " does not exist");
                System.exit(1);
            }

        } else if(Files.isDirectory(Paths.get(directory)) ) {
            log.info("Directory: " + directory);
            ProcessDir processDir = new ProcessDir(Paths.get(directory));
            List<Path> files = processDir.getFiles();
            for(Path path: files){
                process(path, sheet, operation, cmd);
            }
        }
        else {
            log.severe("Directory: " + directory + " does not exist");
            System.exit(1);
        }
    }
    private static void printHelp(Options options){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar insert-row.jar", options);
    }
    private static enum Operation{
        INSERT_ROW,
        REMOVE_ROW,
        CONVERT_GM_TRANSLATE_JSON
    }

    public static void process(Path path, String sheet, Operation operation, CommandLine cmd) throws NotAValidFileException, NotAnXlsxException {
        if( null != sheet){
            log.info("File: " + path.toString());
            switch (operation){
                case INSERT_ROW:
                    InsertRow ir = new InsertRow(path);
                    ir.insertRow(sheet);
                    break;
                case REMOVE_ROW:
                    RemoveRow rr = new RemoveRow(path);
                    rr.removeRow(sheet);
                    break;
                case CONVERT_GM_TRANSLATE_JSON:
                    ToJson toJson = new ToJson(path);
                    JsonStream.setIndentionStep(2);
                    byte[] out = toJson.convert(sheet);
                    String outFile = cmd.getOptionValue("j");
                    Path outPath = Paths.get(outFile);
                    writeToFile(out, outPath);
                    break;
            }
        } else {
            log.info("Adding a row to each sheet");
            switch (operation){
                case INSERT_ROW:
                    InsertRow ir = new InsertRow(path);
                    ir.insertRowAllSheets();
                    break;
                case REMOVE_ROW:
                    RemoveRow rr = new RemoveRow(path);
                    rr.removeRowAllSheets();
                    break;
                case CONVERT_GM_TRANSLATE_JSON:
                    ToJson toJson = new ToJson(path);
                    JsonStream.setIndentionStep(2);
                    byte[] out = toJson.convert();
                    String outFile = cmd.getOptionValue("j");
                    Path outPath = Paths.get(outFile);
                    writeToFile(out, outPath);
                    break;
            }
        }
    }
    private static void writeToFile(byte[] bytes, Path path){
        try(FileOutputStream fos = new FileOutputStream(path.toFile())){
            fos.write(bytes);
        } catch (FileNotFoundException e) {
            log.severe(e.getLocalizedMessage());
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
        }


    }

}