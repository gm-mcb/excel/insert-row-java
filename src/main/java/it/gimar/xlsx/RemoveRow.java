package it.gimar.xlsx;

import it.gimar.xlsx.exception.NotAValidFileException;
import it.gimar.xlsx.exception.NotAnXlsxException;
import lombok.extern.java.Log;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

@Log
public class RemoveRow {
    private final Path path;

    public RemoveRow(Path path) throws NotAnXlsxException, NotAValidFileException {
        this.path = path;
        if(!this.path.toString().endsWith("xlsx")){
            throw new NotAnXlsxException(this.path);
        }
        if(!Files.exists(this.path) || Files.isDirectory(this.path)){
            throw new NotAValidFileException(this.path);
        }
    }

    public void removeRow(String sheet) {
        File file = path.toFile();
        byte[] bytes = new byte[0];
        try(FileInputStream fip = new FileInputStream(file)) {
            XSSFWorkbook workbook = new XSSFWorkbook(fip);
            XSSFSheet xssfSheet = workbook.getSheet(sheet);
            this.removeRow(xssfSheet);
            try(ByteArrayOutputStream baos = new ByteArrayOutputStream()){
                workbook.write(baos);
                bytes = baos.toByteArray();
            }
        } catch (FileNotFoundException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        }
        if(bytes.length > 0) {
            try (FileOutputStream fos = new FileOutputStream(file)) {
                fos.write(bytes);
            } catch (FileNotFoundException e) {
                log.severe(e.getLocalizedMessage());
                e.printStackTrace();
            } catch (IOException e) {
                log.severe(e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
    }
    public void removeRowAllSheets() {
        File file = path.toFile();
        try(FileInputStream fip = new FileInputStream(file)) {
            XSSFWorkbook workbook = new XSSFWorkbook(fip);
            int sheets = workbook.getNumberOfSheets();
            for(int i = 0; i < sheets; i++){
                XSSFSheet sheet = workbook.getSheetAt(i);
                this.removeRow(sheet);
            }
            try(FileOutputStream fos = new FileOutputStream(file)){
                workbook.write(fos);
            }
        } catch (FileNotFoundException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }
    private void removeRow(XSSFSheet sheet){
        if( sheet.getLastRowNum() > 0) {
            XSSFRow row = sheet.getRow(0);
            if (row != null) {
                short firstCell = row.getFirstCellNum();
                if (firstCell >= 0) {
                    log.warning("Remove Row: Not Empty");
                    return;
                }
            }
            sheet.shiftRows(1, sheet.getLastRowNum(), -1, true, true);
            log.info("Remove Row: successfully removed");
        }else {
            log.info("Remove Row: Row is already empty");
        }
    }
}
