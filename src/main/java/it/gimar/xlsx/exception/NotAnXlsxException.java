package it.gimar.xlsx.exception;

import java.nio.file.Path;

public class NotAnXlsxException extends Throwable {
    public NotAnXlsxException(Path path) {
        super(path.toString() + " is not an xlsx");
    }
}
