package it.gimar.xlsx.exception;

import java.nio.file.Path;

public class NotAValidFileException extends Throwable {
    public NotAValidFileException(Path path) {
        super(path.toString() + " is not a directory");
    }
}
