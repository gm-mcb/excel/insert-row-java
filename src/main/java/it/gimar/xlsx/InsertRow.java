package it.gimar.xlsx;

import it.gimar.xlsx.exception.NotAValidFileException;
import it.gimar.xlsx.exception.NotAnXlsxException;
import lombok.extern.java.Log;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

@Log
public class InsertRow {

    private final Path path;
    public InsertRow(Path path) throws NotAValidFileException, NotAnXlsxException {
        this.path = path;
        if(!this.path.toString().endsWith("xlsx")){
            throw new NotAnXlsxException(this.path);
        }
        if(!Files.exists(this.path) || Files.isDirectory(this.path)){
            throw new NotAValidFileException(this.path);
        }

    }

    public void insertRow(String sheet) {
        File file = path.toFile();
        byte[] bytes = new byte[0];
        try(FileInputStream fip = new FileInputStream(file)) {
            XSSFWorkbook workbook = new XSSFWorkbook(fip);
            XSSFSheet xssfSheet = workbook.getSheet(sheet);
            this.insertRow(xssfSheet);
            try(ByteArrayOutputStream baos = new ByteArrayOutputStream()){
                workbook.write(baos);
                bytes = baos.toByteArray();
            }
        } catch (FileNotFoundException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        }
        if(bytes.length > 0) {
            try (FileOutputStream fos = new FileOutputStream(file)) {
                fos.write(bytes);
            } catch (FileNotFoundException e) {
                log.severe(e.getLocalizedMessage());
                e.printStackTrace();
            } catch (IOException e) {
                log.severe(e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
    }
    public void insertRowAllSheets() {
        File file = path.toFile();
        try(FileInputStream fip = new FileInputStream(file)) {
            XSSFWorkbook workbook = new XSSFWorkbook(fip);
            int sheets = workbook.getNumberOfSheets();
            for(int i = 0; i < sheets; i++){
                XSSFSheet sheet = workbook.getSheetAt(i);
                this.insertRow(sheet);
            }
            try(FileOutputStream fos = new FileOutputStream(file)){
                workbook.write(fos);
            }
        } catch (FileNotFoundException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }
    private void removeRow(XSSFSheet sheet){
        if( sheet.getLastRowNum() > 0) {
            XSSFRow row = sheet.getRow(0);
            if (row != null) {
                short firstCell = row.getFirstCellNum();
                if (firstCell >= 0) {
                    sheet.shiftRows(0, sheet.getLastRowNum(), 1, true, true);
                    log.info("Insert Row: Added Row");
                    return;
                }
            }
        }
        log.info("Insert Row: Nothing to Do, Row is already empty");
    }
    private void insertRow(XSSFSheet sheet){
        if( sheet.getLastRowNum() > 0) {
            XSSFRow row = sheet.getRow(0);
            if (row != null) {
                short firstCell = row.getFirstCellNum();
                if (firstCell >= 0) {
                    sheet.shiftRows(0, sheet.getLastRowNum(), 1, true, true);
                    log.info("Insert Row: Added Row");
                    return;
                }
            }
        }
        log.info("Insert Row: Nothing to Do, Row is already empty");
    }

}
