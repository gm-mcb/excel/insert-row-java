package it.gimar.xlsx.convert;

import com.jsoniter.output.JsonStream;
import it.gimar.xlsx.exception.NotAValidFileException;
import it.gimar.xlsx.exception.NotAnXlsxException;
import lombok.extern.java.Log;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@Log
public class ToJson {

    private static final String EMPTY_STRING = "";
    private final Path path;
    public ToJson(Path path) throws NotAValidFileException, NotAnXlsxException {
        this.path = path;
        if(!this.path.toString().endsWith("xlsx")){
            throw new NotAnXlsxException(this.path);
        }
        if(!Files.exists(this.path) || Files.isDirectory(this.path)){
            throw new NotAValidFileException(this.path);
        }
    }


    public byte[] convert() {
        File file = path.toFile();
        byte[] bytes = new byte[0];
        try(FileInputStream fip = new FileInputStream(file)) {
            XSSFWorkbook workbook = new XSSFWorkbook(fip);
            Map<String, Translatsions> map = xssfWorkbookToTranslations(workbook);
            String out = JsonStream.serialize(map);
            log.fine(out);
            bytes = out.getBytes(Charset.forName("UTF-8"));
        } catch (FileNotFoundException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return bytes;
    }
    public byte[] convert(String sheet) {
        File file = path.toFile();
        byte[] bytes = new byte[0];
        try(FileInputStream fip = new FileInputStream(file)) {
            XSSFWorkbook workbook = new XSSFWorkbook(fip);
            XSSFSheet xssfSheet = workbook.getSheet(sheet);
            Translatsions translatsions = xssfSheetToTranslations(xssfSheet);
            String out = JsonStream.serialize(translatsions);
            log.fine(out);
            bytes = out.getBytes(Charset.forName("UTF-8"));
        } catch (FileNotFoundException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return bytes;
    }
    private Map<String, Translatsions> xssfWorkbookToTranslations(XSSFWorkbook workbook){
        Map<String, Translatsions> map = new HashMap<>();
        int sheetCount = workbook.getNumberOfSheets();
        for(int i = 0; i < sheetCount; i++){
            XSSFSheet xssfSheet = workbook.getSheetAt(i);
            GmRows rows = this.xssfSheetToRows(xssfSheet);
            Translatsions translatsions = new Translatsions(rows);
            map.put(xssfSheet.getSheetName(), translatsions);
        }
        return map;
    }
    private Translatsions xssfSheetToTranslations(XSSFSheet xssfSheet){
        GmRows rows = this.xssfSheetToRows(xssfSheet);
        return new Translatsions(rows);
    }

    private GmRows xssfSheetToRows(XSSFSheet sheet) { List<String> headers = new ArrayList<>(); GmRows rows = new GmRows();
        for(int rowNum = 0; rowNum <= sheet.getLastRowNum(); rowNum++){
            XSSFRow xssfRow = sheet.getRow(rowNum);
            if(rowNum == 0){
                headers = xssfHeadersToList(xssfRow);
            }else{
                GmRows.GmRow row = xssfRowToRow(xssfRow, headers);
                rows.add(row);
            }
        }
        return rows;
    }

    private Map<String, List<String>> headersToMap(List<String> headers){
        Map<String, List<String>> map = new TreeMap<>();
        for (String header : headers){
            if(null != header && "" != header){
                map.put(header, new ArrayList<>());
            }
        }
        return map;
    }
    private List<String> xssfHeadersToList(XSSFRow row) {
        List<String> values = new ArrayList<>();
        for(short cellNum = 0; cellNum < row.getLastCellNum(); cellNum++){
            XSSFCell cell = row.getCell(cellNum, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
            values.add(cellValue(cell));
        }
        return values;
    }
    private GmRows.GmRow xssfRowToRow(XSSFRow row, List<String> headers) {
        GmRows.GmRow myRow = new GmRows.GmRow();
        for(short cellNum = 0; cellNum < row.getLastCellNum(); cellNum++){
            XSSFCell cell = row.getCell(cellNum, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
            if(cell == null){
                myRow.addCell(headers.get(cellNum), EMPTY_STRING);
            }else {
                myRow.addCell(headers.get(cellNum), cellValue(cell));
            }
        }
        return myRow;
    }
    private String cellValue(XSSFCell cell){
        switch (cell.getCellType()){
            case NUMERIC:
                return String.valueOf(cell.getNumericCellValue());
            case FORMULA:
                return cell.getStringCellValue();
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case STRING:
                return cell.getStringCellValue();
            case ERROR:
                return EMPTY_STRING;
            case BLANK:
                return EMPTY_STRING;
            case _NONE:
                return cell.getStringCellValue();
            default:
                return EMPTY_STRING;
        }
    }
    private static class GmRows {
        private List<GmRow> rows = new ArrayList<>();

        private void add(GmRow row) {
            this.rows.add(row);
        }

        private static class GmRow{
            private List<GmCell> gmCells = new ArrayList<>();

            public void addCell(String header, String value) {
                this.gmCells.add(new GmCell(header, value));
            }
        }

        private static class GmCell {
            private String header;
            private String value;
            public GmCell(String header, String value){
                this.header = header;
                this.value = value;
            }
        }
    }

    private static class Translatsions{
        List<Translation> records;
        private Translatsions(GmRows gmRows){
            this.records = gmRows.rows.stream()
                .map(row -> new Translation(row))
                .collect(Collectors.toList());
        }

        private static class Translation{
            String key;
            List<NameValue> languages = new ArrayList<>();

            private Translation(GmRows.GmRow row) {
               this.key = row.gmCells.get(0).value;
                this.languages = row.gmCells
                    .subList(1, row.gmCells.size())
               .stream()
               .map(gmCell -> new NameValue(gmCell))
                   .collect(Collectors.toList());
            }
        }

        private static class NameValue{
            String name;
            String value;
            private NameValue(GmRows.GmCell cell){
                this.name = cell.header;
                this.value = cell.value;
            }
        }
    }
}
