package it.gimar.xlsx;

import it.gimar.xlsx.exception.NotAValidFileException;
import it.gimar.xlsx.exception.NotAnXlsxException;
import lombok.extern.java.Log;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import static org.assertj.core.api.Assertions.*;

@Log
public class RemoveRowTest {

    Path resourcePath;
    String sheet = "Foglio1";
    private XSSFWorkbook startWb;

    @Test
    public void removeRowSingleSheetTest() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-removing-single.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        RemoveRow removeRow = new RemoveRow(this.resourcePath);
        XSSFRow startRow = startWb.getSheet(this.sheet).getRow(0);
        assertThat(startRow == null).isTrue();
        removeRow.removeRow(sheet);

        XSSFWorkbook endWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        startRow = endWb.getSheet(this.sheet).getRow(0);
        assertThat(startRow == null).isFalse();
    }
    @Test
    public void removeRowMultiSheetTest() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-removing-multiple.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        RemoveRow removeRow = new RemoveRow(this.resourcePath);
        for(int i = 0; i < startWb.getNumberOfSheets(); i++){
            XSSFRow startRow = startWb.getSheetAt(i).getRow(0);
            assertThat(startRow == null).isTrue();
        }
        removeRow.removeRowAllSheets();

        XSSFWorkbook endWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        for(int i = 0; i < endWb.getNumberOfSheets(); i++){
            XSSFSheet sheet = endWb.getSheetAt(i);
            if(sheet.getLastRowNum() > -1) {
                XSSFRow startRow = sheet.getRow(0);
                assertThat(startRow == null).isFalse();
            }
        }
    }


}
