package it.gimar.xlsx;

import it.gimar.xlsx.exception.NotAValidFileException;
import it.gimar.xlsx.exception.NotAnXlsxException;
import lombok.extern.java.Log;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

@Log
public class InsertRowTest {

    Path resourcePath;
    String sheet = "Foglio1";
    private XSSFWorkbook startWb;

    @Test
    public void insertRowSingleSheetTest() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-adding.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        InsertRow removeRow = new InsertRow(this.resourcePath);
        XSSFRow startRow = startWb.getSheet(this.sheet).getRow(0);
        assertThat(startRow == null).isFalse();
        removeRow.insertRow(sheet);

        XSSFWorkbook endWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        startRow = endWb.getSheet(this.sheet).getRow(0);
        assertThat(startRow == null).isTrue();
    }
    @Test
    public void insertRowMultiSheetTest() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-adding.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        InsertRow removeRow = new InsertRow(this.resourcePath);
        for(int i = 0; i < startWb.getNumberOfSheets(); i++){
            XSSFSheet sheet = startWb.getSheetAt(i);
            if(sheet.getLastRowNum() > -1) {
                XSSFRow startRow = startWb.getSheetAt(i).getRow(0);
                assertThat(startRow == null).isFalse();
            }
        }
        removeRow.insertRowAllSheets();
        XSSFWorkbook endWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        for(int i = 0; i < endWb.getNumberOfSheets(); i++){
            XSSFSheet sheet = endWb.getSheetAt(i);
            if(sheet.getLastRowNum() > -1) {
                XSSFRow startRow = sheet.getRow(0);
                assertThat(startRow == null).isTrue();
            }
        }
    }
}
