package it.gimar.xlsx.convert;

import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;
import it.gimar.xlsx.TestResourceHelper;
import it.gimar.xlsx.TestXlsxHelper;
import it.gimar.xlsx.exception.NotAValidFileException;
import it.gimar.xlsx.exception.NotAnXlsxException;
import lombok.extern.java.Log;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@Log
public class ToJsonTest {

    Path resourcePath;
    String sheet = "Sheet1";
    private XSSFWorkbook startWb;

    @Test
    public void convertSingleSheet_Strange_Many_Sheets_Test() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-json-strange.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        ToJson toJson = new ToJson(this.resourcePath);
        byte[] outBytes = toJson.convert(sheet);
        outBytes = toJson.convert();

        assertThat(outBytes.length).isGreaterThan(0);
        Any json = JsonIterator.deserialize(outBytes);
        List<XSSFSheet> sheets = IntStream.range(0, this.startWb.getNumberOfSheets())
            .mapToObj( i -> this.startWb.getSheetAt(i))
            .collect(Collectors.toList());
        for(XSSFSheet xssfSheet : sheets) {
            Any anySheet = json.asMap().get(xssfSheet.getSheetName());
            Any records = anySheet.get("records");
            assertThat(records).hasSize(this.startWb.getSheet(sheet).getLastRowNum());
            for (Any record : records.asList()) {
                String key = record.toString("key");
                assertThat(key).isNotNull();
                Any languages = record.get("languages");
                assertThat(languages).hasSize(4);
                for (Any language : languages.asList()) {
                    assertThat(language.toString("name")).isNotEmpty();
                    assertThat(language.toString("value")).isNotNull();
                }
            }
        }

    }
    @Test
    public void convertSingleSheet_Strange_Single_Sheet_Test() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-json-strange.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        ToJson toJson = new ToJson(this.resourcePath);
        byte[] outBytes = toJson.convert(sheet);

        assertThat(outBytes.length).isGreaterThan(0);

        Any json = JsonIterator.deserialize(outBytes);
        Any records = json.asMap().get("records");
        assertThat(records).hasSize(this.startWb.getSheet(sheet).getLastRowNum());
        for(Any record : records.asList()){
            String key = record.toString("key");
            assertThat(key).isNotNull();
            Any languages = record.get("languages");
            assertThat(languages).hasSize(4);
            for(Any language : languages.asList()){
                assertThat(language.toString("name")).isNotEmpty();
                assertThat(language.toString("value")).isNotNull();
            }
        }
    }
}
