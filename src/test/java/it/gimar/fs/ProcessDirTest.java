package it.gimar.fs;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class ProcessDirTest {
    Path tempDir;
    @Before
    public void setup() throws IOException {
         tempDir = Files.createTempDirectory("test");
    }
    @After
    public void tearDown() throws IOException {
        Files.list(tempDir)
            .forEach(f -> {
                try {
                    Files.delete(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        Files.deleteIfExists(tempDir);
    }
    @Test
    public void processFile_EmptyDirTest() throws IOException, ProcessDir.NotADirectoryException {
        assertThat(tempDir).exists();
        assertThat(Files.list(tempDir)).isEmpty();
        ProcessDir processDir = new ProcessDir(tempDir);
        List<Path> files = processDir.getFiles();
        assertThat(files).isEmpty();
    }
    @Test
    public void processFile_FullDir_OneTest() throws IOException, ProcessDir.NotADirectoryException {
        assertThat(tempDir).exists();
        assertThat(Files.list(tempDir)).isEmpty();
        Path file = Files.createFile(Paths.get(tempDir.toAbsolutePath().toString(), "test.xlsx"));
        ProcessDir processDir = new ProcessDir(tempDir);
        List<Path> files = processDir.getFiles();
        assertThat(files).hasSize(1);
    }
    @Test
    public void processFile_FullDir_ManyTest() throws IOException, ProcessDir.NotADirectoryException {
        assertThat(tempDir).exists();
        assertThat(Files.list(tempDir)).isEmpty();
        long count = IntStream.range(0, 10)
            .mapToObj(i -> {
                try {
                    Path file = Files.createFile(Paths.get(tempDir.toAbsolutePath().toString(), "test-" + i + ".xlsx"));
                    return file;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }).count();
        ProcessDir processDir = new ProcessDir(tempDir);
        List<Path> files = processDir.getFiles();
        assertThat(files).hasSize(Math.toIntExact(count));
    }
}
